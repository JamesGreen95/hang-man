package com.aprog.hang_man;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;

import java.util.Random;

public class MovingButton extends Button {
    public MovingButton(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setBackgroundResource(R.drawable.menu_button_background);
        this.setGravity(Gravity.CENTER);
        int rightLeftPadding = (int) getResources().getDimension(R.dimen.menu_button_right_left_padding);
        int topBottomPadding = (int) getResources().getDimension(R.dimen.menu_button_top_bottom_padding);
        this.setPadding(rightLeftPadding,topBottomPadding,rightLeftPadding,topBottomPadding);
        this.setTypeface(Typeface.createFromAsset(context.getAssets(), "fonts/CaveatBrush.ttf"));

        float currentX = getX();
        float currentY = getY();
        float newX = currentX+10;
        float newY = currentY+10;

        Animation animation = new TranslateAnimation(currentX,newX,currentY,newY);
        animation.setInterpolator(new AccelerateDecelerateInterpolator());
        animation.setDuration(1500);
        animation.setRepeatCount(Animation.INFINITE);
        animation.setRepeatMode(Animation.REVERSE);
        animation.setStartOffset(new Random().nextInt(1000));
        this.startAnimation(animation);
    }
}
